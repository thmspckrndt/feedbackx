//
//  AnnotationViewController.swift
//  feedbackx
//
//  Created by Thomas on 8/14/14.
//  Copyright (c) 2014 Thomas Pockrandt. All rights reserved.
//


import UIKit

class AnnotationViewController: UIViewController {
    var textView = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // title
        self.title = "Add Concern"
        
        // background
        self.view.backgroundColor = UIColor.blackColor()
        
        // navigationBar
        self.navigationController.navigationBar.topItem.backBarButtonItem = UIBarButtonItem(title: String(), style: .Plain, target: self, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: "submitPost:")
        
        // textView
        self.textView.frame = self.view.bounds
        self.textView.autoresizingMask  = .FlexibleHeight | .FlexibleWidth
        self.textView.font              = UIFont(descriptor: UIFontDescriptor(), size: 23.0)
        self.textView.becomeFirstResponder()
        self.view.addSubview(self.textView)
    }
    
    func submitPost(button: UIBarButtonItem) {
        self.navigationController.pushViewController(SubmittedViewController(), animated: true)
    }
}