//
//  PhotoViewController.swift
//  feedbackx
//
//  Created by Thomas on 8/14/14.
//  Copyright (c) 2014 Thomas Pockrandt. All rights reserved.
//

import UIKit

class PhotoViewController: UITableViewController {
    override init() {
        super.init(style: .Grouped)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // title
        self.title = "Mark the Spot"
        
        // navigationBar
        self.navigationController.navigationBar.topItem.backBarButtonItem = UIBarButtonItem(title: String(), style: .Plain, target: self, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Marked", style: .Plain, target: self, action: "nextViewController:")
    }
    
    func nextViewController(button: UIBarButtonItem) {
        self.navigationController.pushViewController(AnnotationViewController(), animated: true)
    }
}