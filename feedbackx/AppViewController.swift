//
//  AppViewController.swift
//  feedbackx
//
//  Created by Thomas on 8/14/14.
//  Copyright (c) 2014 Thomas Pockrandt. All rights reserved.
//

import UIKit

class AppViewController: UITableViewController {
    var tableViewData: [String: String]?
    
    override init() {
        super.init(style: .Grouped)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // title
        self.title = "Installed Apps"
        
        // navigationBar
        self.navigationController.navigationBar.topItem.backBarButtonItem = UIBarButtonItem(title: String(), style: .Plain, target: self, action: nil)
        
        // searchBar
        let searchController = UISearchController(searchResultsController: self)
        searchController.searchBar.sizeToFit()
        searchController.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        self.tableView.tableHeaderView = searchController.searchBar
        
        // tableViewData
        self.tableViewData = [
            "Uber": "uber",
            "Dash": "dash",
            "Minecraft": "minecraft",
            "Facebook Messenger": "facebook-messenger",
            "Twitter": "twitter",
            "Snapchat": "snapchat",
            "SoundCloud ": "sound-cloud",
            "Flappy Bird": "flappy-bird",
            "Skype": "skype",
            "Beats Music": "beats-music"
        ]
    }
    
    // tableView
    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView!, titleForHeaderInSection section: Int) -> String! {
        switch section {
        case 0:
            return "Report a Bug for"
        default:
            return String()
        }
    }
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if let tableViewData = self.tableViewData {
                return tableViewData.count
            } else {
                return 0
            }
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell = UITableViewCell(style: .Value1, reuseIdentifier: "reuseIdentifier")
        
        cell.textLabel.text = [String](self.tableViewData!.keys)[indexPath.row]
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            default:
                self.navigationController.pushViewController(DetailViewController(), animated: true)
            }
        default:
            break
        }
    }
}
