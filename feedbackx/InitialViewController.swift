//
//  InitialViewController.swift
//  feedbackx
//
//  Created by Thomas on 8/14/14.
//  Copyright (c) 2014 Thomas Pockrandt. All rights reserved.
//

import UIKit

class InitialViewController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // navigationBar
        self.navigationBar.barStyle     = .Black
        self.navigationBar.tintColor    = UIColor.yellowColor()
        
        // pushViewController
        self.pushViewController(MainViewController(), animated: false)
    }
}