//
//  MainViewController.swift
//  feedbackx
//
//  Created by Thomas on 8/14/14.
//  Copyright (c) 2014 Thomas Pockrandt. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    override init() {
        super.init(style: .Grouped)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // title
        self.title = "FeedbackX"
        
        // navigationBar
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Login", style: .Plain, target: self, action: nil)
    }
    
    // tableView
    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView!, titleForHeaderInSection section: Int) -> String! {
        switch section {
        case 0:
            return "I want to"
        default:
            return String()
        }
    }
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell = UITableViewCell(style: .Value1, reuseIdentifier: "reuseIdentifier")
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell.textLabel.text = "Report a Bug"
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            case 1:
                cell.textLabel.text = "Create an Issue"
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            case 2:
                cell.textLabel.text = "Submit a Feature Request"
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            case 3:
                cell.textLabel.text = "Ask a Question"
                cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                cell.textLabel.text = "About"
                cell.accessoryType = UITableViewCellAccessoryType.DetailButton
                break
            default:
                break
            }
        default:
            break
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            default:
                self.navigationController.pushViewController(AppViewController(), animated: true)
            }
        default:
            break
        }
    }
}