//
//  AppDelegate.swift
//  feedbackx
//
//  Created by Thomas on 8/14/14.
//  Copyright (c) 2014 Thomas Pockrandt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(application: UIApplication!, didFinishLaunchingWithOptions launchOptions: NSDictionary!) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window!.rootViewController = InitialViewController()
        self.window!.makeKeyAndVisible()
        
        return true
    }
}

